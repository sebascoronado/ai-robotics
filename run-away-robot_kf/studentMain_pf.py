from math import *
from matrix import * # Check the matrix.py tab to see how this works.
import random
import pdb

# helper function to map all angles onto [-pi, pi]
def angle_trunc(a):
    while a < 0.0:
        a += pi * 2
    return ((a + pi) % (pi * 2)) - pi

class robot:

    def __init__(self, x = 0.0, y = 0.0, heading = 0.0, turning = 2*pi/10, distance = 1.0):
        """This function is called when you create a new robot. It sets some of
        the attributes of the robot, either to their default values or to the values
        specified when it is created."""
        self.x = x
        self.y = y
        self.heading = heading
        self.turning = turning # only applies to target robots who constantly move in a circle
        self.distance = distance # only applies to target bot, who always moves at same speed.
        self.turning_noise    = 0.0
        self.distance_noise    = 0.0
        self.measurement_noise = 0.0


    def set_noise(self, new_t_noise, new_d_noise, new_m_noise):
        """This lets us change the noise parameters, which can be very
        helpful when using particle filters."""
        self.turning_noise    = float(new_t_noise)
        self.distance_noise    = float(new_d_noise)
        self.measurement_noise = float(new_m_noise)


    def move(self, turning, distance, tolerance = 0.001, max_turning_angle = pi):
        """This function turns the robot and then moves it forward."""
        # apply noise, this doesn't change anything if turning_noise
        # and distance_noise are zero.
        turning = random.gauss(turning, self.turning_noise)
        distance = random.gauss(distance, self.distance_noise)

        # truncate to fit physical limitations
        turning = max(-max_turning_angle, turning)
        turning = min( max_turning_angle, turning)
        distance = max(0.0, distance)

        # Execute motion
        self.heading += turning
        self.heading = angle_trunc(self.heading)
        self.x += distance * cos(self.heading)
        self.y += distance * sin(self.heading)

    def move_in_circle(self):
        """This function is used to advance the runaway target bot."""
        self.move(self.turning, self.distance)

    def sense(self):
        """This function represents the robot sensing its location. When
        measurements are noisy, this will return a value that is close to,
        but not necessarily equal to, the robot's (x, y) position."""
        return (random.gauss(self.x, self.measurement_noise),
                random.gauss(self.y, self.measurement_noise))

    def __repr__(self):
        """This allows us to print a robot's position"""
        return '[%.5f, %.5f]'  % (self.x, self.y)

    def measurement_prob(self, measurement):
        # calculates how likely a measurement should be
        return 1.0/(sqrt((self.x - measurement[0]) ** 2 + (self.y - measurement[1]) ** 2))



# This is the function you have to write. Note that measurement is a
# single (x, y) point. This function will have to be called multiple
# times before you have enough information to accurately predict the
# next position. The OTHER variable that your function returns will be
# passed back to your function the next time it is called. You can use
# this to keep track of important information over time.
def estimate_next_pos(measurement, OTHER = None):
    """Estimate the next (x, y) position of the wandering Traxbot
    based on noisy (x, y) measurements."""

    # You must return xy_estimate (x, y), and OTHER (even if it is None)
    # in this order for grading purposes.
    # particles
    N = 500
    #vars
    d_avg = 0.0
    r_avg = 0.0
    rotation = [0]
    #pdb.set_trace()
    if not OTHER:
        #init other
        OTHER = [[],[]]
        #initialize the particles
        p = []
        for i in range(N):
            x = robot(measurement[0], measurement[1])
            x.set_noise(0.05, 0.05, 5.0)
            p.append(x)
        #pdb.set_trace()
        OTHER[0] = p
        xy_estimate = (measurement[0], measurement[1])

    OTHER[1].append(measurement)
    measure_a = OTHER[1]
    partics_a = OTHER[0]

    if len(measure_a) > 1:
        xy_estimate = (measurement[0], measurement[1])
        #calculate distance and rotation using measures array
        #distance
        distance = [distance_between(measure_a[i+1],measure_a[i]) for i in range(len(measure_a)-1)]
        d_avg = sum(distance) / float(len(distance))

        #rotation
        rotation = [atan2(measure_a[i+1][1]-measure_a[i][1], measure_a[i+1][0]-measure_a[i][0]) for i in range(len(measure_a)-1)]
        if len(rotation) > 1:
            rotation_1 = [(rotation[i+1]-rotation[i])%(2*pi) for i in range(len(rotation)-1)]
            r_avg = sum(rotation_1) / float(len(rotation_1))

        #pdb.set_trace()
        p2 = []
        for i in range(N):
            partics_a[i].move(r_avg, d_avg)
            p2.append(partics_a[i])
        partics_a = p2

        next_p = (measurement[0]+d_avg*cos(rotation[len(rotation)-1]+r_avg),measurement[1]+d_avg*sin(rotation[len(rotation)-1]+r_avg))
        w = []
        for i in range(N):
            w.append(partics_a[i].measurement_prob(next_p))

        p3 = []
        index = int(random.random() * N)
        beta = 0.0
        mw = max(w)
        for i in range(N):
            beta += random.random() * 2.0 * mw
            while beta > w[index]:
                beta -= w[index]
                index = (index + 1) % N
            p3.append(partics_a[index])
        OTHER[0] = p3
        #pdb.set_trace()
        xy_estimate = [p3[i].x/len(p3),p3[i].y/len(p3)]
        xy_estimate = (measurement[0]+xy_estimate[0]+d_avg*cos(rotation[len(rotation)-1]+r_avg),measurement[1]+ xy_estimate[1]+d_avg*sin(rotation[len(rotation)-1]+r_avg))
    return xy_estimate, OTHER

# A helper function you may find useful.
def distance_between(point1, point2):
    """Computes distance between point1 and point2. Points are (x, y) pairs."""
    x1, y1 = point1
    x2, y2 = point2
    return sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

# This is here to give you a sense for how we will be running and grading
# your code. Note that the OTHER variable allows you to store any
# information that you want.
def demo_grading(estimate_next_pos_fcn, target_bot, OTHER = None):
    if False:
        localized = False
        distance_tolerance = 0.01 * target_bot.distance
        ctr = 0
        # if you haven't localized the target bot, make a guess about the next
        # position, then we move the bot and compare your guess to the true
        # next position. When you are close enough, we stop checking.
        while not localized and ctr <= 1000:
            ctr += 1
            measurement = target_bot.sense()
            position_guess, OTHER = estimate_next_pos_fcn(measurement, OTHER)
            target_bot.move_in_circle()
            true_position = (target_bot.x, target_bot.y)
            error = distance_between(position_guess, true_position)
            if error <= distance_tolerance:
                print "You got it right! It took you ", ctr, " steps to localize."
                localized = True
            if ctr == 1000:
                print "Sorry, it took you too many steps to localize the target."
        return localized
    else:
        localized = False
        distance_tolerance = 0.01 * target_bot.distance
        ctr = 0
        # if you haven't localized the target bot, make a guess about the next
        # position, then we move the bot and compare your guess to the true
        # next position. When you are close enough, we stop checking.
        #For Visualization
        import turtle    #You need to run this locally to use the turtle module
        window = turtle.Screen()
        window.bgcolor('white')
        size_multiplier= 25.0  #change Size of animation
        broken_robot = turtle.Turtle()
        broken_robot.shape('turtle')
        broken_robot.color('green')
        broken_robot.resizemode('user')
        broken_robot.shapesize(0.1, 0.1, 0.1)
        measured_broken_robot = turtle.Turtle()
        measured_broken_robot.shape('circle')
        measured_broken_robot.color('red')
        measured_broken_robot.resizemode('user')
        measured_broken_robot.shapesize(0.1, 0.1, 0.1)
        prediction = turtle.Turtle()
        prediction.shape('arrow')
        prediction.color('blue')
        prediction.resizemode('user')
        prediction.shapesize(0.1, 0.1, 0.1)
        prediction.penup()
        broken_robot.penup()
        measured_broken_robot.penup()
        #End of Visualization
        while not localized and ctr <= 1000:
            ctr += 1
            measurement = target_bot.sense()
            position_guess, OTHER = estimate_next_pos_fcn(measurement, OTHER)
            target_bot.move_in_circle()
            true_position = (target_bot.x, target_bot.y)
            error = distance_between(position_guess, true_position)
            if error <= distance_tolerance:
                print "You got it right! It took you ", ctr, " steps to localize."
                localized = True
            if ctr == 1000:
                print "Sorry, it took you too many steps to localize the target."
            #More Visualization
            measured_broken_robot.setheading(target_bot.heading*180/pi)
            measured_broken_robot.goto(measurement[0]*size_multiplier, measurement[1]*size_multiplier-200)
            measured_broken_robot.stamp()
            broken_robot.setheading(target_bot.heading*180/pi)
            broken_robot.goto(target_bot.x*size_multiplier, target_bot.y*size_multiplier-200)
            broken_robot.stamp()
            prediction.setheading(target_bot.heading*180/pi)
            prediction.goto(position_guess[0]*size_multiplier, position_guess[1]*size_multiplier-200)
            prediction.stamp()
            #End of Visualization
        return localized
# This is a demo for what a strategy could look like. This one isn't very good.
def naive_next_pos(measurement, OTHER = None):
    """This strategy records the first reported position of the target and
    assumes that eventually the target bot will eventually return to that
    position, so it always guesses that the first position will be the next."""
    if not OTHER: # this is the first measurement
        OTHER = measurement
    xy_estimate = OTHER
    return xy_estimate, OTHER

# This is how we create a target bot. Check the robot.py file to understand
# How the robot class behaves.
test_target = robot(2.1, 4.3, 0.5, 2*pi / 34.0, 1.5)
measurement_noise = 0.05 * test_target.distance
test_target.set_noise(0.0, 0.0, measurement_noise)

demo_grading(estimate_next_pos, test_target)




