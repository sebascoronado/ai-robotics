#measure uncertainty
R = matrix([[0.1, 0.],
            [0., 0.1]])
#state transition matrix
F = matrix([[1., 1., 0.],
            [0., 1., 0.],
            [0., 0., 1.]])
# measurement function
H = matrix([[1., 0., 0.],
            [0., 0., 1.]])
#Identity
I = matrix([[1., 0., 0.],
            [0., 1., 0.],
            [0., 0., 1.]])

def next_move(hunter_position, hunter_heading, target_measurement, max_distance, OTHER = None):
    # estimate target pos
    if not OTHER:
        # estimate heading and distance
        X = matrix([[0.],
                    [0.],
                    [0.]])
        P = matrix([[1000., 0., 0.],
                    [0., 1000., 0.],
                    [0., 0., 1000.]])
        OTHER = [[], X, P]
    OTHER[0].append(target_measurement)
    # measure heading and distance
    if len(OTHER[0]) == 1:
        heading = 0
        distance = 0
    else:
        #pdb.set_trace()
        mr = OTHER[0]
        heading = atan2(mr[-1][1]-mr[-2][1], mr[-1][0]-mr[-2][0]) % (2*pi)
        distance = distance_between(mr[-1],mr[-2])
        OTHER[0].pop(0)

    X = OTHER[1]
    P = OTHER[2]
    #update heading
    pre_heading = X.value[0][0]
    for d in [-1, 0, 1]:
        diff = (int(pre_heading / (2 * pi)) + d) * (2 * pi)
        if abs(heading + diff - pre_heading) < pi:
            heading += diff
            break
    # measurement update
    Z = matrix([[heading],
                [distance]])
    Y = Z - (H * X)
    S = H * P * H.transpose() + R
    K = P * H.transpose() * S.inverse()
    X = X + (K * Y)
    P = (I - K * H) * P
    # prediction
    X = F * X
    P = F * P * F.transpose()

    OTHER[1] = X
    OTHER[2] = P

    heading = X.value[0][0]
    distance = X.value[2][0]

    xy_estimate = (target_measurement[0] + distance * cos(heading) ,
                   target_measurement[1] + distance * sin(heading))

    # estimate distance to target pos
    distance = distance_between(hunter_position, xy_estimate)
    if distance > max_distance:
        distance = max_distance

    # estimate heading to target pos
    heading = atan2 (xy_estimate[1] - hunter_position[1],
                     xy_estimate[0] - hunter_position[0])

    heading = angle_trunc(heading)
    turning = heading - hunter_heading
    turning = angle_trunc(turning)

    return turning, distance, OTHER