
# ----------
# User Instructions:
#
# Create a function compute_value which returns
# a grid of values. The value of a cell is the minimum
# number of moves required to get from the cell to the goal.
#
# If a cell is a wall or it is impossible to reach the goal from a cell,
# assign that cell a value of 99.
# ----------
import pdb
grid = [[0, 1, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 1, 0]]
goal = [len(grid)-1, len(grid[0])-1]
cost = 1 # the cost associated with moving from a cell to an adjacent one

delta = [[-1, 0 ], # go up
         [ 0, -1], # go left
         [ 1, 0 ], # go down
         [ 0, 1 ]] # go right

delta_name = ['^', '<', 'v', '>']

def compute_value(grid,goal,cost):
    # ----------------------------------------
    # insert code below
    # ----------------------------------------

    # make sure your function returns a grid of values as
    # demonstrated in the previous video.
    value = [[99 for i in grid[0]] for y in grid]
    nodes = [[0, goal[0], goal[1]]]
    value[goal[0]][goal[1]] = 0

    while len(nodes) > 0:
        #pdb.set_trace()
        next_n = nodes.pop(0)
        x = next_n[1]
        y = next_n[2]
        p_cost = next_n[0]
        for i in delta:
            x2 = x + i[0]
            y2 = y + i[1]

            if x2 >= 0 and y2 >= 0 and x2 < len(grid) and y2 < len(grid[0]):
                if (grid[x2][y2] == 0):
                    if p_cost+cost < value[x2][y2]:
                        nodes.append([p_cost+cost, x2, y2])
                        value[x2][y2] = p_cost + cost
    return value
value = compute_value(grid,goal,cost)
for col in range(len(value)):
    print value[col]