# ----------
# User Instructions:
#
# Implement the function optimum_policy2D below.
#
# You are given a car in grid with initial state
# init. Your task is to compute and return the car's
# optimal path to the position specified in goal;
# the costs for each motion are as defined in cost.
#
# There are four motion directions: up, left, down, and right.
# Increasing the index in this array corresponds to making a
# a left turn, and decreasing the index corresponds to making a
# right turn.
import pdb
forward = [[-1,  0], # go up
           [ 0, -1], # go left
           [ 1,  0], # go down
           [ 0,  1]] # go right
forward_name = ['up', 'left', 'down', 'right']

# action has 3 values: right turn, no turn, left turn
action = [-1, 0, 1]
action_name = ['R', '#', 'L']

# EXAMPLE INPUTS:
# grid format:
#     0 = navigable space
#     1 = unnavigable space
grid = [[1, 1, 1, 0, 0, 0],
        [1, 1, 1, 0, 1, 0],
        [0, 0, 0, 0, 0, 0],
        [1, 1, 1, 0, 1, 1],
        [1, 1, 1, 0, 1, 1]]

init = [4, 3, 0] # given in the form [row,col,direction]
                 # direction = 0: up
                 #             1: left
                 #             2: down
                 #             3: right

goal = [2, 0] # given in the form [row,col]

cost = [20, 1, 2] # cost has 3 values, corresponding to making
                  # a right turn, no turn, and a left turn

# EXAMPLE OUTPUT:
# calling optimum_policy2D with the given parameters should return
# [[' ', ' ', ' ', 'R', '#', 'R'],
#  [' ', ' ', ' ', '#', ' ', '#'],
#  ['*', '#', '#', '#', '#', 'R'],
#  [' ', ' ', ' ', '#', ' ', ' '],
#  [' ', ' ', ' ', '#', ' ', ' ']]
# ----------

# ----------------------------------------
# modify code below
# ----------------------------------------
def getKey(key):
  return key[3]

def optimum_policy2D(grid,init,goal,cost):

    #init vars
    possible_solutions = []
    possible_solutions.append([init[0],init[1], init[2], 0, []])
    solution = [[99 for i in range(len(grid[0]))] for y in range(len(grid))]
    policy2D = [[' ' for i in range(len(grid[0]))] for y in range(len(grid))]
    visited = [[[] for i in range(len(grid[0]))] for y in range(len(grid))]
    policy2D_1 = [[' ' for i in range(len(grid[0]))] for y in range(len(grid))]
    #iterate throuh possible solutions
    x = -1
    y = -1
    current_direction = 0
    while possible_solutions:
      possible_solutions = sorted(possible_solutions, key=getKey)
      current = possible_solutions.pop(0)

      if x == -1 and y == -1:
        x = init[0]
        y = init[1]

      x = current[0]
      y = current[1]
      current_direction = current[2]
      cost_sum = current[3]
      stack = current[4]

      if x == goal[0] and y == goal[1]:
        policy2D[x][y] = '*'
        current[4].append([x,y,'*'])
        break

      for i,act in enumerate(action):
        #pdb.set_trace()
        index = (current_direction + act) % len(forward)
        new_x = x + forward[index][0]
        new_y = y + forward[index][1]
        new_dir = forward.index(forward[index])
        new_cost = cost_sum + cost[i]
        if new_x >= 0 and new_y >= 0 and new_x < len(grid) and new_y < len(grid[0]):
          if forward_name[new_dir] not in visited[new_x][new_y] and grid[new_x][new_y] != 1:
            new_stack = stack[:]
            new_stack.append([x,y,action_name[i]])
            visited[x][y].append(forward_name[new_dir]+str(x)+str(y))
            possible_solutions.append([new_x, new_y, new_dir, new_cost, new_stack])
            if new_cost < solution[x][y]:
              policy2D [x][y] = action_name[i]
              solution[x][y] = new_cost
      # for i in range(len(solution)):
      #   print solution[i]
      # for i in range(len(policy2D)):
      #   print policy2D[i]

    # copy solution
    for i in range(len(current[4])):
      x = current[4][i][0]
      y = current[4][i][1]
      policy2D_1[x][y] = current[4][i][2]

    return policy2D_1

sol = optimum_policy2D(grid,init,goal,cost)
#print sol
for i in range(len(sol)):
  print sol[i]