from robot import *  # Check the robot.py tab to see how this works.
from math import *
from matrix import * # Check the matrix.py tab to see how this works.
import random

# measurement uncertainty
R = matrix([[1., 0.],
            [0., 1.]])
#state transition matrix
F = matrix([[1., 1., 0.],
            [0., 1., 0.],
            [0., 0., 1.]])
# measurement function
H = matrix([[1., 0., 0.],
            [0., 0., 1.]])
#Identity
I = matrix([[1., 0., 0.],
            [0., 1., 0.],
            [0., 0., 1.]])



def estimate_next_pos(measurement, OTHER=None):
    if not OTHER:
        # estimate heading and distance
        X = matrix([[0.],
                    [0.],
                    [0.]])
        P = matrix([[1000., 0., 0.],
                    [0., 1000., 0.],
                    [0., 0., 1000.]])
        OTHER = [[], X, P]
    OTHER[0].append(measurement)
    # measure heading and distance
    if len(OTHER[0]) == 1:
        heading = 0
        distance = 0
    else:
        mr = OTHER[0]
        heading = angle_trunc(atan2(mr[-1][1]-mr[-2][1], mr[-1][0]-mr[-2][0]) % (2*pi))
        distance = distance_between(mr[-1],mr[-2])
        OTHER[0].pop(0)

    X = OTHER[1]
    P = OTHER[2]
    #update heading
    previous_heading = X.value[0][0]
    diff = [(int(previous_heading / (2 * pi)) + d) * (2 * pi) for d in [-2,-1, 0, 1, 2]]
    for i in diff:
        if abs(heading + i - previous_heading) < pi:
            heading += i
            break
    # measurement update
    Z = matrix([[heading],
                [distance]])
    Y = Z - (H * X)
    S = H * P * H.transpose() + R
    K = P * H.transpose() * S.inverse()
    X = X + (K * Y)
    P = (I - K * H) * P
    # prediction
    X = F * X
    P = F * P * F.transpose()

    OTHER[1] = X
    OTHER[2] = P

    heading = X.value[0][0]
    distance = X.value[2][0]
    xy_estimate = (measurement[0] + distance * cos(heading) ,
                   measurement[1] + distance * sin(heading))

    return xy_estimate, OTHER

# A helper function you may find useful.
def distance_between(point1, point2):
    """Computes distance between point1 and point2. Points are (x, y) pairs."""
    x1, y1 = point1
    x2, y2 = point2
    return sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)