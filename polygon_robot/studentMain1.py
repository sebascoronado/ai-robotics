from robot import *
from math import *
from matrix import *
import random
import pdb

def estimate_next_pos(measurement, OTHER = None):
    """Estimate the next (x, y) position of the wandering Traxbot
    based on noisy (x, y) measurements."""

    xy_estimate = (measurement[0], measurement[1])
    #pdb.set_trace()
    if OTHER == None:
        OTHER = []
    OTHER.append(measurement)
    d_avg = 0.0
    r_avg = 0.0
    rotation = [0]

    if len(OTHER) > 1:
        #distance
        distance = [distance_between(OTHER[i+1],OTHER[i]) for i in range(len(OTHER)-1)]
        d_avg = sum(distance) / float(len(distance))

        #rotation
        rotation = [atan2(OTHER[i+1][1]-OTHER[i][1], OTHER[i+1][0]-OTHER[i][0]) for i in range(len(OTHER)-1)]
        if len(rotation) > 1:
            rotation_1 = [(rotation[i+1]-rotation[i])%(2*pi) for i in range(len(rotation)-1)]
            r_avg = sum(rotation_1) / float(len(rotation_1))

    xy_estimate = (measurement[0]+d_avg*cos(rotation[len(rotation)-1]+r_avg), measurement[1]+d_avg*sin(rotation[len(rotation)-1]+r_avg))
    return xy_estimate, OTHER

# A helper function you may find useful.
def distance_between(point1, point2):
    """Computes distance between point1 and point2. Points are (x, y) pairs."""
    x1, y1 = point1
    x2, y2 = point2
    return sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
