#
# === Introduction ===
#
# In this problem, you will build a planner that helps a robot
#   find the best path through a warehouse filled with boxes
#   that it has to pick up and deliver to a dropzone.
#
# Your file must be called `partA.py` and must have a class
#   called `DeliveryPlanner`.
# This class must have an `__init__` function that takes three
#   arguments: `self`, `warehouse`, and `todo`.
# The class must also have a function called `plan_delivery` that
#   takes a single argument, `self`.
#
# === Input Specifications ===
#
# `warehouse` will be a list of m strings, each with n characters,
#   corresponding to the layout of the warehouse. The warehouse is an
#   m x n grid. warehouse[i][j] corresponds to the spot in the ith row
#   and jth column of the warehouse, where the 0th row is the northern
#   end of the warehouse and the 0th column is the western end.
#
# The characters in each string will be one of the following:
#
# '.' (period) : traversable space. The robot may enter from any adjacent space.
# '#' (hash) : a wall. The robot cannot enter this space.
# '@' (dropzone): the starting point for the robot and the space where all boxes must be delivered.
#   The dropzone may be traversed like a '.' space.
# [0-9a-zA-Z] (any alphanumeric character) : a box. At most one of each alphanumeric character
#   will be present in the warehouse (meaning there will be at most 62 boxes). A box may not
#   be traversed, but if the robot is adjacent to the box, the robot can pick up the box.
#   Once the box has been removed, the space functions as a '.' space.
#
# For example,
#   warehouse = ['1#2',
#                '.#.',
#                '..@']
#   is a 3x3 warehouse.
#   - The dropzone is at the warehouse cell in row 2, column 2.
#   - Box '1' is located in the warehouse cell in row 0, column 0.
#   - Box '2' is located in the warehouse cell in row 0, column 2.
#   - There are walls in the warehouse cells in row 0, column 1 and row 1, column 1.
#   - The remaining five warehouse cells contain empty space.
#
# The argument `todo` is a list of alphanumeric characters giving the order in which the
#   boxes must be delivered to the dropzone. For example, if
#   todo = ['1','2']
#   is given with the above example `warehouse`, then the robot must first deliver box '1'
#   to the dropzone, and then the robot must deliver box '2' to the dropzone.
#
# === Rules for Movement ===
#
# - Two spaces are considered adjacent if they share an edge or a corner.
# - The robot may move horizontally or vertically at a cost of 2 per move.
# - The robot may move diagonally at a cost of 3 per move.
# - The robot may not move outside the warehouse.
# - The warehouse does not "wrap" around.
# - As described earlier, the robot may pick up a box that is in an adjacent square.
# - The cost to pick up a box is 4, regardless of the direction the box is relative to the robot.
# - While holding a box, the robot may not pick up another box.
# - The robot may put a box down on an adjacent empty space ('.') or the dropzone ('@') at a cost
#   of 2 (regardless of the direction in which the robot puts down the box).
# - If a box is placed on the '@' space, it is considered delivered and is removed from the ware-
#   house.
# - The warehouse will be arranged so that it is always possible for the robot to move to the
#   next box on the todo list without having to rearrange any other boxes.
#
# An illegal move will incur a cost of 100, and the robot will not move (the standard costs for a
#   move will not be additionally incurred). Illegal moves include:
# - attempting to move to a nonadjacent, nonexistent, or occupied space
# - attempting to pick up a nonadjacent or nonexistent box
# - attempting to pick up a box while holding one already
# - attempting to put down a box on a nonadjacent, nonexistent, or occupied space
# - attempting to put down a box while not holding one
#
# === Output Specifications ===
#
# `plan_delivery` should return a LIST of moves that minimizes the total cost of completing
#   the task successfully.
# Each move should be a string formatted as follows:
#
# 'move {i} {j}', where '{i}' is replaced by the row-coordinate of the space the robot moves
#   to and '{j}' is replaced by the column-coordinate of the space the robot moves to
#
# 'lift {x}', where '{x}' is replaced by the alphanumeric character of the box being picked up
#
# 'down {i} {j}', where '{i}' is replaced by the row-coordinate of the space the robot puts
#   the box, and '{j}' is replaced by the column-coordinate of the space the robot puts the box
#
# For example, for the values of `warehouse` and `todo` given previously (reproduced below):
#   warehouse = ['1#2',
#                '.#.',
#                '..@']
#   todo = ['1','2']
# `plan_delivery` might return the following:
#   ['move 2 1',
#    'move 1 0',
#    'lift 1',
#    'move 2 1',
#    'down 2 2',
#    'move 1 2',
#    'lift 2',
#    'down 2 2']
#
# === Grading ===
#
# - Your planner will be graded against a set of test cases, each equally weighted.
# - If your planner returns a list of moves of total cost that is K times the minimum cost of
#   successfully completing the task, you will receive 1/K of the credit for that test case.
# - Otherwise, you will receive no credit for that test case. This could happen for one of several
#   reasons including (but not necessarily limited to):
#   - plan_delivery's moves do not deliver the boxes in the correct order.
#   - plan_delivery's output is not a list of strings in the prescribed format.
#   - plan_delivery does not return an output within the prescribed time limit.
#   - Your code raises an exception.
#
# === Additional Info ===
#
# - You may add additional classes and functions as needed provided they are all in the file `partA.py`.
# - Upload partA.py to Project 2 on T-Square in the Assignments section. Do not put it into an
#   archive with other files.
# - Your partA.py file must not execute any code when imported.
# - Ask any questions about the directions or specifications on Piazza.
#
import pdb

class DeliveryPlanner:

    def __init__(self, warehouse, todo):
        self.warehouse = warehouse
        self.todo = todo
        self.horizontallyM = 2
        self.diagonallyM = 3
        self.pickUpBoxM = 4
        self.putDownBoxM = 2
        self.delta = [[-1, 0 ], # go up
                 [ 0, -1], # go left
                 [ 1, 0 ], # go down
                 [ 0, 1 ], # go right
                 [-1, 1 ], # go up right
                 [-1, -1], # go up left
                 [1, 1], # go down right
                 [1, -1 ]] # go down left

    def getKey(self, item):
        return item[0]

    def adjacent(self, start, goal):
        result = False

        if start[0] == goal[0] and start[1] == goal[1]:
            result = True

        for a in range(len(self.delta)):
            x2 = start[0] + self.delta[a][0]
            y2 = start[1] + self.delta[a][1]

            if x2 == goal[0] and y2 == goal[1]:
                result = True
                break
        return result

    def findEmptyAdjacentCells(self, position):
        result = []
        for a in range(len(self.delta)):
            x2 = position[0] + self.delta[a][0]
            y2 = position[1] + self.delta[a][1]


            #adjacent cells inside matrix
            if  x2 >= 0 and x2 < len(self.warehouse) and y2 >=0 and y2 < len(self.warehouse[0]):
                #allowed to move
                if self.warehouse[x2][y2] == '.':
                    result += [[x2,y2]]
        return result

    def findAdjacentCellTo(self, adjacentCells, position):
        for i in range(len(adjacentCells)):
            if self.adjacent(adjacentCells[i], position):
                return adjacentCells[i]
        return []

    def plan_delivery(self):
        #vars
        moves = []
        boxes = {}
        findPathToBox = True
        dropzone = 0
        position = 0
        boxIndex = 0
        bag = []
        # map objects
        for col in range(len(self.warehouse)):
            print self.warehouse[col]
            for row in range(len(self.warehouse[col])):
                if self.warehouse[col][row] == '@':
                    dropzone = [col,row]
                    position = [col,row]
                if self.warehouse[col][row].isalnum():
                    boxes[self.warehouse[col][row]] = [col,row]


        #compute optimum policy from all locations to goal
        #pdb.set_trace()
        self.optimum = self.find_optimum_policy(dropzone)
        #print self.optimum

        # compute answer
        while boxIndex < len(boxes):
           #find paths
           #pdb.set_trace()
           forward_path, reverse_path, endPos = self.find_path(dropzone, boxes[self.todo[boxIndex]], position)

           #back to dropzone if necessary
           if len(forward_path) > 0:
                goal = [int(s) for s in forward_path[0].split() if s.isdigit()]
                adjacent = self.adjacent(position, goal)
                if not adjacent:
                    moves += ['move %d %d' % (dropzone[0],dropzone[1])]
           # find box
           if len(bag) > 0:
                if len(forward_path) > 0:
                    forward_path = [forward_path[0]] + [bag[0]] + forward_path[1:]
                #move to adjancent cell that is also adjacent to goal
                else:
                    #pdb.set_trace()
                    emptyAdjacentCells = self.findEmptyAdjacentCells(position)
                    freecell = emptyAdjacentCells[0]#self.findAdjacentCellTo(emptyAdjacentCells, boxes[self.todo[boxIndex]])
                    t = ['move %d %d' % (freecell[0], freecell[1])]
                    forward_path = t + [bag[0]]
                bag = []
           moves += forward_path
           moves += ['lift %c' % self.todo[boxIndex]]

           #modify warehouse matrix
           t_str = list(self.warehouse[boxes[self.todo[boxIndex]][0]])
           t_str[boxes[self.todo[boxIndex]][1]] = '.'
           self.warehouse[boxes[self.todo[boxIndex]][0]] = "".join(t_str)

           position = endPos
           # deliver box
           moves += reverse_path
           if position[0] == dropzone[0] and position[1] == dropzone[1]:
                bag += ['down %d %d' % (dropzone[0], dropzone[1])]
           else:
                moves += ['down %d %d' % (dropzone[0], dropzone[1])]

           boxIndex += 1
           #self.optimum = self.find_optimum_policy(dropzone)
        return moves

    # find best path using pre_cal function
    def find_path(self, start, goal, position):
        moves = []
        forward_path = []
        reverse_path = []
        forward_path2 = []
        reverse_path2 = []
        #find path
        closed = [[0 for col in range(len(self.warehouse[0]))] for row in range(len(self.warehouse))]
        closed[goal[0]][goal[1]] = 1

        x = goal[0]
        y = goal[1]
        g = 0

        open = [[g, x, y]]
        found = False

        while not found:
            if len(open) == 0:
                break
            else:
                #found best key
                open = sorted(open, key=self.getKey)
                next1 = open.pop(0)
                x = next1[1]
                y = next1[2]
                g = next1[0]

                #add to move list
                moves += [[x,y]]

                #goal found
                if start[0] == x and start[1] == y:
                    found = True
                else:
                    #iterate through all possible moves
                    for a in range(len(self.delta)):
                        x2 = x + self.delta[a][0]
                        y2 = y + self.delta[a][1]
                        #adjacent cells inside matrix
                        if  x2 >= 0 and x2 < len(self.warehouse) and y2 >=0 and y2 < len(self.warehouse[0]):
                            #not visited
                            if closed[x2][y2] == 0:
                                #cell is transversable
                                if self.warehouse[x2][y2] == '.' or self.warehouse[x2][y2] == '@':
                                    g2 = self.optimum[x2][y2]
                                    open.append([g2, x2, y2])
                                    closed[x2][y2] = 1

        pdb.set_trace()
        #format paths
        forward_path2 = moves[1:-1]
        reverse_path2 = forward_path2[1:]
        forward_path2 = forward_path2[::-1]

        #set last position
        if not reverse_path2:
            if not forward_path2:
                lastPos = position
            else:
                lastPos = forward_path2[-1]
        else:
            lastPos = reverse_path2[-1]

        #remove first move if we already are there
        if len(forward_path2) > 0 and position[0] == forward_path2[0][0] and position[1] == forward_path2[0][1]:
            forward_path2 = forward_path2[1:]

        for i in range(len(forward_path2)):
            forward_path += ['move %d %d' % (forward_path2[i][0],forward_path2[i][1])]
        for i in range(len(reverse_path2)):
            reverse_path += ['move %d %d' % (reverse_path2[i][0],reverse_path2[i][1])]

        return forward_path, reverse_path, lastPos

    def find_optimum_policy(self, goal):
        #define variables
        value = [[9999 for row in range(len(self.warehouse[0]))] for col in range(len(self.warehouse))]
        change = True

        while change:
            change = False

            #iterate through all matrix
            for x in range(len(self.warehouse)):
                for y in range(len(self.warehouse[0])):
                    #pdb.set_trace()
                    #set goal value function matrix to 0
                    if goal[0] == x and goal[1] == y:
                        #modify value of goal to 0
                        if value[x][y] > 0:
                            #pdb.set_trace()
                            value[x][y] = 0
                            change = True
                    elif self.warehouse[x][y] == '.' or self.warehouse[x][y] == '@':
                        cost = 0
                        # calculate possible adjancent move and cost
                        for a in range(len(self.delta)):
                            #set move x2 y2
                            x2 = x + self.delta[a][0]
                            y2 = y + self.delta[a][1]

                            #set cost value
                            if a <= 3:
                                cost = 2
                            else:
                                cost = 3
                        #check if new move is inside matrix
                            if x2 >= 0 and x2 < len(self.warehouse) and y2 >= 0 and y2 < len(self.warehouse[0]):
                                #if possible to visit
                                if self.warehouse[x2][y2] == '.' or self.warehouse[x2][y2] == '@':
                                    #pdb.set_trace()
                                    v2 = value[x2][y2] + cost
                                    if v2 < value[x][y]:
                                        #action[x][y] = delta_name[a]
                                        change = True
                                        value[x][y] = v2
        return value

