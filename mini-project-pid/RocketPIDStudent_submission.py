# Optimize your PID parameters here:
pressure_tau_p = -1.
pressure_tau_d = -2.5

rocket_tau_p = -52.33403147844316
rocket_tau_i = 12.312309143985365
rocket_tau_d = 0.0


def pressure_pd_solution(delta_t, current_pressure, data):
    """Student solution to maintain LOX pressure to the turbopump at a level of 100.

    Args:
        delta_t (float): Time step length.
        current_pressure (float): Current pressure level of the turbopump.
        data (dict): Data passed through out run.  Additional data can be added and existing values modified.
            'ErrorP': Proportional error.  Initialized to 0.0
            'ErrorD': Derivative error.  Initialized to 0.0
    """

    # TODO: implement PD solution here
    error = 100 - current_pressure
    adjust_pressure = - delta_t*pressure_tau_p*error - delta_t*pressure_tau_d*(error - data['ErrorP'])
    data['ErrorP'] = error
    data['ErrorD'] = error - data['ErrorP']
    return adjust_pressure, data


# def rocket_pid_solution_twiddle(delta_t, current_velocity, optimal_velocity, data, taus):
#     """Student solution for maintaining rocket throttle through out the launch based on an optimal flight path

#     Args:
#         delta_t (float): Time step length.
#         current_velocity (float): Current velocity of rocket.
#         optimal_velocity (float): Optimal velocity of rocket.
#         data (dict): Data passed through out run.  Additional data can be added and existing values modified.
#             'ErrorP': Proportional error.  Initialized to 0.0
#             'ErrorI': Integral error.  Initialized to 0.0
#             'ErrorD': Derivative error.  Initialized to 0.0

#     Returns:
#         Throttle to set, data dictionary to be passed through run.
#     """

#     # TODO: implement PID Solution here
#     error = optimal_velocity - current_velocity
#     #print optimal_velocity
#     data['ErrorI'] = error - data['ErrorP']
#     throttle = -delta_t*taus[0]* error + \
#                -delta_t*taus[1]* (error - data['ErrorP']) + \
#                -delta_t*taus[2]* (error + data['ErrorD'])
#     data['ErrorP'] = error
#     data['ErrorD'] += error

#     return throttle, data

def rocket_pid_solution(delta_t, current_velocity, optimal_velocity, data):
    """Student solution for maintaining rocket throttle through out the launch based on an optimal flight path

    Args:
        delta_t (float): Time step length.
        current_velocity (float): Current velocity of rocket.
        optimal_velocity (float): Optimal velocity of rocket.
        data (dict): Data passed through out run.  Additional data can be added and existing values modified.
            'ErrorP': Proportional error.  Initialized to 0.0
            'ErrorI': Integral error.  Initialized to 0.0
            'ErrorD': Derivative error.  Initialized to 0.0

    Returns:
        Throttle to set, data dictionary to be passed through run.
    """

    # TODO: implement PID Solution here
    error = optimal_velocity - current_velocity
    #print optimal_velocity
    data['ErrorI'] = error - data['ErrorP']
    throttle = -delta_t*rocket_tau_p * error + \
               -delta_t*rocket_tau_i * (error - data['ErrorP']) + \
               -delta_t*rocket_tau_d * (error + data['ErrorD'])
    data['ErrorP'] = error
    data['ErrorD'] += error

    return throttle, data
